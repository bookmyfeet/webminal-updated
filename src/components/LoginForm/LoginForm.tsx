import React, { useState, useContext } from "react";
import { supabase } from "../../lib/supabase";
import Spinner from "../Spinner";
import { userContext } from "../../lib/context";
import { useHistory } from "@docusaurus/router";

const LoginForm = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [loginRequested, setLoginRequested ] = useState(false);
  const [email, setEmail] = useState("");
  const onEmailSubmit = () => {
    setIsLoading(true);
    supabase.auth
      .signIn({ email })
      .then((res) => {
        console.log(res);
        setLoginRequested(true);
      })
      .catch((err) => {
        alert("An error occured. Please try again");

        console.log(err);
      })
      .finally(() => setIsLoading(false));
  };

  const onGithubSubmit = () => {
    setIsLoading(true);
    supabase.auth
      .signIn({ provider: "github" }, { redirectTo: "./welcome" })
      .then((res) => {
        console.log(res);
        setLoginRequested(true);
      })
      .catch((err) => {
        alert("An error occured. Please try again");

        console.log(err);
      })
      .finally(() => setIsLoading(false));
  };

  if (isLoading)
    return (
      <div className="h-[100vh] w-[99%] flex justify-center items-center overflow-hidden">
        <div className="w-[77vmin] aspect-square ">
          <Spinner />
        </div>
      </div>
    );

  return (
    <>{!loginRequested ? (
      <div className="flex relative max-w-xl bg-gray-100 p-8 lg:p-12 py-8 rounded-md mx-auto flex-col space-y-4 m-8 border-2 border-slate-400">
        <h1 className='text-2xl font-bold mb-6'>Please Sign up / Sign In</h1>
        <a className='cursor-pointer'>
          <div className="absolute right-2 top-2">
            <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd" /></svg>
          </div>
        </a>
        <div className="flex py-0 items-center justify-center space-x-4 mt-3">
          <button
            onClick={onGithubSubmit}
            className="flex items-center py-2 px-4 text-sm uppercase rounded bg-[#171515] text-white border border-transparent hover:border-transparent hover:opacity-90 shadow-md hover:shadow-lg font-medium transition transform hover:-translate-y-0.5"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 16 16"
              className="w-6 h-6 mr-3"
            >
              <path
                fillRule="evenodd"
                fill="white"
                d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"
              ></path>
            </svg>
            Continue With Github
          </button>
        </div>
        <div>
          <p className="text-center text-base my-4 text-gray-700 font-light">
            Or sign in with MagicLink
          </p>
          <form className="flex flex-col">
            <label htmlFor="email" className="mt-4 opacity-80">Enter Email Address</label>
            <div className="flex border mt-2 rounded-md shadow-sm focus-within:border-blue-500 items-center bg-white">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-7 w-7 ml-3 text-gray-400 p-1"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"
                />
                <path
                  d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"
                />
              </svg>
              <input
                id='email'
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                type="email"
                placeholder="Your email"
                required
                className="py-2 w-full px-2 outline-none bg-transparent" />
            </div>
            <div className="flex items-center mt-8">
              <button
                onClick={onEmailSubmit}
                className="text-white py-2 px-4 uppercase rounded bg-blue-500 hover:bg-blue-600 shadow hover:shadow-lg font-medium transition transform hover:-translate-y-0.5 w-full"
              >
                Sign in
              </button>
            </div>
          </form>

        </div>
      </div>) : (
        <div className="text-2xl text-green-500 font-bold text-center m-20">check you email!</div>
      )}
    </>
  );
};

export default LoginForm;
