//@ts-ignore
import styles from "./spinner.module.css";
import React from "react";
const Spinner = () => {
  return (
    <div className={styles.spinner}>
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

export default Spinner;
