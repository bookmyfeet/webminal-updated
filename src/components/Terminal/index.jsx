import styles from "./styles.module.css";
import React, { useEffect, useLayoutEffect, useRef, useState } from "react";
import { useLocation } from "@docusaurus/router";
import { supabase } from "../../lib/supabase";
import  md5 from 'md5';


const iframeHtml = `<iframe src='//www.webminal.org/terminal/proxy/index/'/>`;

const Terminal = () => {
  const [active, setActive] = useState(false);
  const pip = useRef();
  const iframeWrapper = useRef();
  const [theme, setTheme] = useState(
    document.querySelector("html").getAttribute("data-theme")
  );
  const path = useLocation();

  useEffect(() => console.log("rerender"));
  //so initially it is 30 vh
  const [heightWrapper, setHeightWrapper] = useState(
    (window.innerHeight / 100) * 30 + "px"
  );
  useEffect(() => {
    var observer = new MutationObserver(function (mutations) {
      mutations.forEach(function (mutation) {
        if (mutation.type === "attributes") {
          setTheme(document.querySelector("html").getAttribute("data-theme"));
        }
      });
    });

    observer.observe(document.querySelector("html"), {
      attributes: true, //configure it to listen to attribute changes
    });

    const mouseMoveHandler = (ev) => {
      //so it is mobile
      if (ev.changedTouches) ev.clientY = ev.changedTouches[0].clientY;
      if (ev.clientY > window.window.innerHeight / 10)
        setHeightWrapper(window.innerHeight - ev.clientY + "px");
    };

    const dragStartHandler = () => {
      document.addEventListener("mousemove", mouseMoveHandler);
      document.addEventListener("touchmove", mouseMoveHandler);
      console.log("started");
    };

    const dragEndHandler = () => {
      document.removeEventListener("mousemove", mouseMoveHandler);
      document.removeEventListener("touchmove", mouseMoveHandler);
    };

    pip?.current.addEventListener("touchstart", dragStartHandler);
    pip?.current.addEventListener("mousedown", dragStartHandler);

    window.addEventListener("mouseup", dragEndHandler);
    window.addEventListener("touchend", dragEndHandler);

    const handler = function (button) {
      setActive((prev) => !prev);
    };
    window.toogleTerminal = handler;

    return () => {
      pip?.current.removeEventListener("touchstart", dragStartHandler);
      pip?.current.removeEventListener("mousedown", dragStartHandler);

      window.removeEventListener("mouseup", dragEndHandler);
      window.removeEventListener("touchend", dragEndHandler);

      observer.disconnect();

      delete window.toogleTerminal;
    };
  }, []);

  useLayoutEffect(() => {
    document.body.style.setProperty(
      "--toogle-content",
      "'" + (active === false ? "Open Terminal" : "Close Terminal") + "'"
    );
  }, [path.pathname, active]);

  const [session, setSession] = useState(null);

  useEffect(() => {
    const session = supabase.auth.session();

    const { data: authListener } = supabase.auth.onAuthStateChange(
      async (event, session) => {
        if (session) setSession(session);
      }
    );
    if (session) setSession(session);
    return () => {
      authListener?.unsubscribe();
    };
  }, [session?.user]);

  useEffect(() => {
    if (
      iframeWrapper.current.children[0] &&
      session &&
      session["access_token"]
    ) {
      console.log("HERE!!!");
      iframeWrapper.current.children[0];
      iframeWrapper.current.children[0].setAttribute(
        "src",
        "//www.webminal.org/terminal/proxy/index/" +
          encodeURI("?id=" + session["access_token"])
      );
      console.log(
        "//www.webminal.org/terminal/proxy/index/" +
          encodeURI("?id=" + session["access_token"])
      );
    }
    console.log("iframeWrapper", iframeWrapper.current.children[0]);
  }, [session && session["access_token"]]);

  return (
    <div
      style={{ "--height": heightWrapper }}
      className={`${styles["terminal-wrapper"]} ${
        styles["terminal-" + theme]
      } ${active && styles["terminal-active"]}`}
    >
      <div ref={pip} className={styles["terminal-height-pip"]}></div>
      <div
        ref={iframeWrapper}
        className={`${styles["terminal-inner"]}`}
        dangerouslySetInnerHTML={{ __html: iframeHtml }}
      ></div>
    </div>
  );
};

export default Terminal;
