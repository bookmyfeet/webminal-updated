import { createContext } from "react";
import { User } from "@supabase/supabase-js";

export const userContext = createContext<User | null>(null);
