import siteConfig from "@generated/docusaurus.config";
import { createClient } from "@supabase/supabase-js";

const REACT_APP_SUPABASE_URL = siteConfig.customFields?.SUPABASE_URL as string ?? "";
const REACT_APP_SUPABASE_KEY = siteConfig.customFields?.SUPABASE_KEY as string ?? "";

export const supabase = createClient(
  REACT_APP_SUPABASE_URL,
  REACT_APP_SUPABASE_KEY,
  {
    persistSession: true,
  }
);
