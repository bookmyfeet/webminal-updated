import React from "react";
import clsx from "clsx";
import Link from "@docusaurus/Link";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import Layout from "@theme/Layout";
import HomepageFeatures from "../components/HomepageFeatures";
import { VscTerminalLinux } from "react-icons/vsc";
import GlowingBox from "../components/GlowingBox";
import './globals.css'
import SignInForm from "../components/LoginForm/LoginForm";
export default function Home(): JSX.Element {
  const { siteConfig } = useDocusaurusContext();

  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />"
    >
      <main style={{ background: '#f2f2f2' }}>
        <div className="background-hero pt-10 p-2">
          <div className='sectionHeader'>
            <div className="container poppins">
              <div className="flex mobilrev">
                <div className="leftpart leftfirsts">
                  <h1 className="poppins">
                    The <span className="orangeth">site</span> what ®
                    <br />
                    you want ---
                    <br />
                    fast and <span className="purpthing"> easy</span>.
                  </h1>
                  <div className="flex">
                    <div className="lef_t">
                      <p className="firstsecm">
                        <strong>
                          We let you build any website
                        </strong>
                        you can imagine with the full power of HTML, CSS, and Javascript
                      </p>
                      <div className="buttonscont specialbo">
                        <a className="buynow" href="#">
                          Get started
                        </a>

                      </div>
                    </div>
                    <div className="righ_t">
                      <button className="playbtn">
                        <img src="img/triarrow.svg" alt="img" />
                      </button>
                    </div>
                  </div>



                </div>
                <div className="rightimg">
                  <div className="cardhea">
                    <img src="img/secimg.png" alt="img" />
                  </div>
                </div>
              </div>
              <div className="importpart flex spacebetween aligncenter">

                <div className="iconplace flex mobilrev">
                  <div className="iconsetit">
                    <div className="flex aligncenter">
                      <div className="iconthingh">
                        <img src="img/cashico.png" alt="img" />
                      </div>
                      <div className="textitemic">
                        101 Crash
                        <br />
                        course
                      </div>
                    </div>
                  </div>
                  <div className="iconsetit">
                    <div className="flex aligncenter">
                      <div className="iconthingh">
                        <img src="img/settingico.png" alt="img" />
                      </div>
                      <div className="textitemic">
                        21 Day
                        <br />
                        portfolio
                      </div>
                    </div>
                  </div>
                  <div className="iconsetit iconsetitlast">
                    <div className="flex aligncenter">
                      <div className="iconthingh">
                        <img src="img/paramico.png" alt="img" />
                      </div>
                      <div className="textitemic">
                        Building a
                        <br />
                        business website
                      </div>
                    </div>
                  </div>
                </div>
                <div className="buttontobottom">
                  <a href="#space"></a>
                </div>
              </div>

            </div>
          </div>
        </div>
        <section id='space' style={{ paddingTop: "4rem" }} className="container max-w-screen-lg ">
          <div className="w-100 justify-center mt-5 h-auto  flex flex-col md:flex-row">
            <div className="flex flex-col grow  justify-center content-center text-center sm:w-full md:w-3/6 p-5">
              <h1
                className="text-7xl font-bold "
                style={{
                  background:
                    "linear-gradient(to right, var(--ifm-color-secondary-darker),var(--ifm-color-primary-darkest))",
                  WebkitTextFillColor: "transparent",
                  WebkitBackgroundClip: "text",
                }}
              >
                Write videos <br />
                in React.
              </h1>
              <p className=" text-left">
                Use your React knowledge to create real MP4 videos. Scale your
                video production using server-side rendering and
                parametrization.
              </p>
            </div>
            <div className="grow sm:w-full md:w-3/6 hover:scale-125 transition-transform">
              <GlowingBox>
                <div className="flex w-full h-full items-center justify-center bg-white">
                  <span>Some things with very cool terminal</span>
                </div>
              </GlowingBox>
            </div>
          </div>
          <br />
          <br />
          <div className="bg-slate-200 mt-6 rounded-md">
            <h2 className="text-center text-4xl p-3">Some text</h2>
            <div
              className="  content-center p-2 grid justify-between mb-5 "
              style={{
                gridTemplateColumns: "repeat(auto-fill, 100px)",
              }}
            >
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>{" "}
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>{" "}
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>{" "}
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>
            </div>
          </div>
          <br />
          <br />
          <div className="container max-w-screen-lg mb-5">
            <div className="w-100 justify-center mt-5 h-auto  flex flex-col md:flex-row">
              <div className="grow sm:w-full md:w-3/6 hover:scale-125 transition-transform">
                <GlowingBox>
                  <div className="flex w-full h-full items-center justify-center bg-white">
                    <span>Some things with very cool terminal</span>
                  </div>
                </GlowingBox>
              </div>
              <div className="flex flex-col grow  justify-center content-center text-center sm:w-full md:w-3/6 p-5">
                <h1
                  className="text-7xl font-bold "
                  style={{
                    background:
                      "linear-gradient(to right, var(--ifm-color-primary-darkest),var(--ifm-color-secondary-darker))",
                    WebkitTextFillColor: "transparent",
                    WebkitBackgroundClip: "text",
                  }}
                >
                  Write videos <br />
                  in React.
                </h1>
                <p className=" text-left">
                  Use your React knowledge to create real MP4 videos. Scale your
                  video production using server-side rendering and
                  parametrization.
                </p>
              </div>
            </div>
          </div>
        </section>
        <br />
        <br />
      </main>
    </Layout>
  );
}
