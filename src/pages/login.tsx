import Layout from "@theme/Layout";
import React from "react";
import LoginForm from "../components/LoginForm/LoginForm";

const Auth = () => {
  return (
    <Layout
      title={`Auth`}
      description="Description will go into a meta tag in <head />"
    >
      <LoginForm />
    </Layout>
  );
};

export default Auth;
