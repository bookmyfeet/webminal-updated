import React, { useContext, useEffect, useState } from "react";
import Layout from "@theme/Layout";
import { supabase } from "../lib/supabase";
import { SubmitHandler, useForm } from "react-hook-form";
import useIsBrowser from "@docusaurus/useIsBrowser";
import { Md5 } from "ts-md5";
import { userContext } from "../lib/context";
import { useHistory } from "@docusaurus/router";

type UserProfile = {
  id?: string;
  created_at?: string;
  firstname: string;
  lastname: string;
  username: string;
  bio: string;
  avatarurl: string;
};

function deepEqual(x: any, y: any): boolean {
  const ok = Object.keys,
    tx = typeof x,
    ty = typeof y;
  return x && y && tx === "object" && tx === ty
    ? ok(x).length === ok(y).length &&
        ok(x).every((key) => deepEqual(x[key], y[key]))
    : x === y;
}

const settings = () => {
  if (!useIsBrowser()) return null;

  const isFirstTime = async (id: string) => {
    if (!id) return false;
    const { count, error } = await supabase
      .from("profiles")
      .select("id", { count: "exact" })
      .eq("id", id);
    return !Boolean(count);
  };

  let user = useContext(userContext);
  const password = Md5.hashStr(user!.id).substring(0, 6);
  const [editing, setEditing] = React.useState(false);
  const [firstTime, setFirstTime] = useState(false);
  isFirstTime(user!.id).then((yes) => {
    setFirstTime(yes);
    if (yes) setEditing(yes);
  });
  const [avatar, setAvatar] = useState<any>(null);
  const [passwordVisible, setPasswordVisible] = React.useState(false);
  let userProfile: UserProfile;
  let {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm<UserProfile>({
    // @ts-ignore
    defaultValues: userProfile,
  });

  (async () => {
    const { data, error } = await supabase
      .from<UserProfile>("profiles")
      .select("*")
      .eq("id", user!.id);
    if (!error && !editing) {
      userProfile = data![0];
      setValue("firstname", userProfile.firstname);
      setValue("lastname", userProfile.lastname);
      setValue("username", userProfile.username);
      setValue("bio", userProfile.bio);
    }
  })();

  // TODO: fix this
  const onSubmit: SubmitHandler<UserProfile> = async (formData) => {
    setEditing(false);
    //@ts-ignore
    let avatarurl;
    if (firstTime) {
      const fileExt = avatar.name.split(".").pop();
      let { data: imageData, error: imageError } = await supabase.storage
        .from("avatars")
        .upload(user!.id + "." + fileExt, avatar);
      if (imageError) {
        alert("error uploading image");
      } else {
        imageData?.Key ? (avatarurl = imageData.Key) : null;
      }
    }
    if (avatarurl) formData.avatarurl = avatarurl;
    let { data, error } = await supabase.from<UserProfile>("profiles").upsert({
      ...formData,
      id: user!.id,
    });
    if (error) {
      alert("Failed to save changes");
      return;
    }
    setValue("firstname", data![0].firstname);
    setValue("lastname", data![0].lastname);
    setValue("username", data![0].username);
    setValue("bio", data![0].bio);
    setEditing(false);
  };

  useEffect(() => {
    if (!user) useHistory().push("/");
  });

  return (
    <Layout title="Settings">
      <div className="">
        <div className="container mx-auto">
          <div className="inputs w-full max-w-2xl p-6 mx-auto">
            <h2 className="text-4xl font-bold">Account Settings</h2>
            <div className="mt-6 border-t border-gray-400 pt-4">
              <div className="w-full md:w-full px-3 mb-6">
                <label
                  className="block text-gray-600 text-base font-bold mb-2"
                  htmlFor="email"
                >
                  email address
                </label>
                <p className="text-xl font-semibold px-1 ml-2" id="email">
                  {user ? user.email : "NA"}
                </p>
              </div>
              <div className="w-full md:w-full pl-3 mb-6">
                <label className="block text-gray-600 text-base font-bold mb-2">
                  password
                </label>
                <div className="flex justify-between flex-wrap">
                  <p
                    className={`font-semibold text-xl px-1 my-1 ml-3 ${
                      passwordVisible ? "" : " select-none blur-sm"
                    }`}
                  >
                    {password}
                  </p>
                  <button
                    className="bg-red-300 min-w-max font-semibold text-sm border-red-900 border-2 p-1 my-1 ml-2 rounded-md"
                    type="button"
                    onClick={() => setPasswordVisible(!passwordVisible)}
                  >
                    {passwordVisible ? (
                      <div>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 20 20"
                          fill="currentColor"
                          className="w-5 h-5 inline"
                        >
                          <path
                            fill-rule="evenodd"
                            d="M3.28 2.22a.75.75 0 00-1.06 1.06l14.5 14.5a.75.75 0 101.06-1.06l-1.745-1.745a10.029 10.029 0 003.3-4.38 1.651 1.651 0 000-1.185A10.004 10.004 0 009.999 3a9.956 9.956 0 00-4.744 1.194L3.28 2.22zM7.752 6.69l1.092 1.092a2.5 2.5 0 013.374 3.373l1.091 1.092a4 4 0 00-5.557-5.557z"
                            clip-rule="evenodd"
                          />
                          <path d="M10.748 13.93l2.523 2.523a9.987 9.987 0 01-3.27.547c-4.258 0-7.894-2.66-9.337-6.41a1.651 1.651 0 010-1.186A10.007 10.007 0 012.839 6.02L6.07 9.252a4 4 0 004.678 4.678z" />
                        </svg>
                        <button type="button" className="pl-1">
                          hide
                        </button>
                      </div>
                    ) : (
                      <div>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 20 20"
                          fill="currentColor"
                          className="w-5 h-5 inline"
                        >
                          <path d="M10 12.5a2.5 2.5 0 100-5 2.5 2.5 0 000 5z" />
                          <path
                            fill-rule="evenodd"
                            d="M.664 10.59a1.651 1.651 0 010-1.186A10.004 10.004 0 0110 3c4.257 0 7.893 2.66 9.336 6.41.147.381.146.804 0 1.186A10.004 10.004 0 0110 17c-4.257 0-7.893-2.66-9.336-6.41zM14 10a4 4 0 11-8 0 4 4 0 018 0z"
                            clip-rule="evenodd"
                          />
                        </svg>
                        <button type="button" className="pl-1">
                          reveal
                        </button>
                      </div>
                    )}
                  </button>
                </div>
              </div>
              {/* edit user information form */}
              <form className="personal w-full border-t border-gray-400 pt-4">
                {firstTime ? (
                  <div className="w-full text-2xl bg-yellow-300 font-bold text-center m-4 rounded-md p-2">
                    fill in your details
                  </div>
                ) : (
                  <></>
                )}
                <div className="flex flex-row w-full gap-4 align-middle">
                  <h2 className="text-2xl text-gray-900 font-bold">
                    User Details
                  </h2>
                  <div
                    className={`${
                      editing ? "grayscale opacity-60" : ""
                    } cursor-pointer p-1 h-fit rounded-md bg-green-400 border-2 border-green-800 font-semibold px-2 text-sm`}
                    onClick={() => setEditing(true)}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      className="w-4 h-4 inline"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125"
                      />
                    </svg>
                    <button type="button" disabled={editing} className="pl-1">
                      edit
                    </button>
                  </div>
                </div>

                <div className="flex items-center justify-between mt-4">
                  <div className="w-full md:w-1/2 px-3 mb-6">
                    <label className="block text-gray-600 text-base font-semibold mb-2">
                      first name
                    </label>
                    <input
                      className="appearance-none block w-full bg-white text-gray-700 border border-gray-400 shadow-inner rounded-md py-3 px-4 leading-tight font-medium focus:outline-none  focus:border-gray-500"
                      {...register("firstname", { required: true })}
                      disabled={!editing}
                    />
                    {errors.firstname && (
                      <span className="text-red-500 text-xs italic">
                        last name can't be empty
                      </span>
                    )}
                  </div>
                  <div className="w-full md:w-1/2 px-3 mb-6">
                    <label className="block text-gray-600 text-base font-semibold mb-2">
                      last name
                    </label>
                    <input
                      className={`appearance-none block w-full bg-white text-gray-700 border border-gray-400 shadow-inner rounded-md py-3 px-4 leading-tight font-medium focus:outline-none  focus:border-gray-500 ${
                        editing ? "disabled" : ""
                      }`}
                      {...register("lastname", { required: true })}
                      disabled={!editing}
                    />
                    {errors.lastname && (
                      <span className="text-red-500 text-xs italic">
                        last name can't be empty
                      </span>
                    )}
                  </div>
                </div>

                <div className="w-full md:w-full px-3 mb-6">
                  <label className="block text-gray-600 text-base font-semibold mb-2">
                    user name
                  </label>
                  {firstTime && (
                    <span className="text-xs text-slate-500">
                      can only be set once
                    </span>
                  )}
                  <input
                    className="appearance-none block w-full bg-white text-gray-700 border border-gray-400 shadow-inner rounded-md py-3 px-4 leading-tight font-medium focus:outline-none  focus:border-gray-500"
                    {...register("username", { required: true })}
                    disabled={!editing || !firstTime}
                  />
                  {errors.username && (
                    <span className="text-red-500 text-xs italic">
                      username can't be empty
                    </span>
                  )}
                </div>

                <div className="w-full md:w-full px-3 mb-6">
                  <label className="block text-gray-600 text-base font-semibold mb-2">
                    bio
                  </label>
                  <textarea
                    className=" rounded-md border resize-y text-gray-700 w-full h-20 py-2 px-3 shadow-inner border-gray-400 font-medium focus:outline-none"
                    {...register("bio", { required: true })}
                    disabled={!editing}
                  />
                  {errors.bio && (
                    <span className="text-red-500 text-xs italic">
                      last name can't be empty
                    </span>
                  )}
                </div>

                <div className="w-full md:w-full px-3 mb-6">
                  <label className="block text-gray-600 text-base font-semibold mb-2">
                    avatar
                  </label>
                  {avatar && (
                    <img
                      src={URL.createObjectURL(avatar)}
                      alt="avatar"
                      className="h-20 w-20 inline rounded-full m-2 border-2 border-slate-600"
                    />
                  )}
                  <input
                    type="file"
                    accept=".jpg,.jpeg,.png"
                    className=" rounded-md border resize-y text-gray-700 py-2 px-3 shadow-inner border-gray-400 font-medium focus:outline-none"
                    name="avatarurl"
                    onChange={(e) => setAvatar(e.target.files![0])}
                    disabled={!editing}
                    required={firstTime}
                  />
                </div>

                <div className="flex justify-end gap-3">
                  <div
                    className={`${
                      editing && !firstTime ? "" : "hidden"
                    } cursor-pointer rounded-md bg-red-300 border-2 border-red-900 font-semibold px-2 p-1 text-sm`}
                    onClick={() => setEditing(false)}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      className="w-5 h-5 inline"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M6 18L18 6M6 6l12 12"
                      />
                    </svg>
                    <button type="button" className="pl-1">
                      cancel
                    </button>
                  </div>
                  <div
                    className={`${
                      editing ? "" : "hidden"
                    } cursor-pointer rounded-md bg-green-400 border-2 border-green-800 font-semibold px-2 p-1 text-sm`}
                    onClick={handleSubmit(onSubmit)}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      className="w-5 h-5 inline"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M4.5 12.75l6 6 9-13.5"
                      />
                    </svg>
                    <button type="button" className="pl-1">
                      save
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default settings;
