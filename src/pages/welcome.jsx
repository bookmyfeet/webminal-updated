import React, { useEffect, useRef, useState } from "react";
import Layout from "@theme/Layout";
import { supabase } from "../lib/supabase";
import { useHistory } from "@docusaurus/router";
import Spinner from "../components/Spinner";
import { AiOutlineUser, AiOutlineChrome } from "react-icons/ai";

export default function Welcome() {
  const imageRef = useRef(null);
  const [username, setUsername] = useState("");
  const [website, setWebiste] = useState("");
  const [user] = useState(() => supabase.auth.user());
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const checkIfFirstTime = async () => {
      return await supabase
        .from("profiles")
        .select("id,  avatarurl")
        .filter("id", "eq", user.id);
    };
    alert(JSON.stringify(user, null, 2))
    if (user) {
      checkIfFirstTime().then((res) => {
        //so it is alraedy in db
        if (res.data.length !== 0) {
          history.push("/");
        }
      });
    } else {
      history.push("/auth");
    }
  }, []);

  const onSubmit = async () => {
    if (
      username == "" ||
      !imageRef.current.files ||
      !imageRef.current.files[0] ||
      !/^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/.test(
        website
      )
    ) {
      alert("Please fill correct data");
      return;
    }
    setIsLoading(true);

    try {
      const image = imageRef.current.files[0];
      const fileExt = image.name.split(".").pop();
      const user = supabase.auth.user();
      const imageResponse = await supabase.storage
        .from("avatars")
        .upload(user.id + "." + fileExt, image);

      const allRes = await supabase.from("profiles").insert({
        id: user.id,
        website,
        username,
        avatarurl: imageResponse.data.Key,
      });
      console.log(allRes);

      history.push("/");
    } catch (error) {
      console.log(error);
      alert(error.message);
    } finally {
      setIsLoading(false);
    }
  };
  if (isLoading)
    return (
      <div className="h-screen w-[99%] flex justify-center items-center overflow-hidden">
        <div className="w-[77vmin] aspect-square ">
          <Spinner />
        </div>
      </div>
    );
  return (
    <Layout
      title={`Welcome`}
      description="Description will go into a meta tag in <head />"
    >
      <main className="h-screen flex flex-col items-center justify-center">
        <div className="p-5 flex flex-col items-center justify-center gap-3.5 border-2 border-gray-900">
          <div className="flex w-full justify-center">
            <AiOutlineUser size="2rem" />

            <input
              placeholder="username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              type="text"
            />
          </div>

          <div className="flex w-full justify-center">
            <AiOutlineChrome size="2rem" />

            <input
              placeholder="website.com"
              value={website}
              onChange={(e) => setWebiste(e.target.value)}
              type="text"
            />
          </div>
          <label>
            Choose avatar
            <br />
            <input ref={imageRef} type="file" accept=".jpg,.jpeg,.png" />
          </label>
          <label className="text-center">
            <span>Be careful! You cant change data after that.</span>
            <br />
            <button
              className="border-lime-600 bg-lime-600 border-2 p-5 rounded-full"
              onClick={onSubmit}
            >
              Submit
            </button>
          </label>
        </div>
      </main>
    </Layout>
  );
}
