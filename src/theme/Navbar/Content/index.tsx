import React, { useContext } from "react";
import Content from "@theme-original/Navbar/Content";
import { userContext } from "../../../lib/context";
import { useHistory } from "@docusaurus/router";

export default function ContentWrapper(props: any) {
  const user = useContext(userContext);
  const history = useHistory();
  const terminalBtnStyle = "#terminal-button:after{content:var(--toogle-content,'Open Terminal');}";

  const onLogOut = () => {
    localStorage.clear();
    location.replace("/login");
  };

  const onSettingsOpen = () => history.push("/settings");

  return (
    <>
      <div
        style={{ maxWidth: "1430px" }}
        className="items-center justify-between flex w-full mx-auto"
      >
        {/* <Content /> */}
        <div className="flex space-x-4 w-full align-middle justify-center">

          <div className="absolute left-2">
            <a href="/">
            <img src="img/logo.svg" alt="logo" className="w-8 inline"/>
            <div className="font-bold text-base inline pl-1">My Site</div>
            </a>
          </div>

          <div className="">
            <a href="/docs/intro" className="px-4 !no-underline">Tutorial</a>
            <a href="/blog" className="px-4 !no-underline">Blog</a>
            {/* @ts-ignore */}
            <button className="px-4" id='terminal-button' onClick={() => window.toogleTerminal(this)}><style>{terminalBtnStyle}</style></button>
          </div>
          <div className="absolute right-0">
          {user ? (
            <div className="flex cursor-pointer  group relative ml-auto !w-fit items-center">
              <span>{user.email?.split("@")[0] || ""}</span>
              <div className="absolute group-hover:flex flex-col hidden top-full w-32 bg-white right-0 shadow-md rounded-sm pt-2">
                <div className="p-2 truncate border-b">{user.email || ""}</div>
                <button
                  className="py-1 h-full text-center"
                  onClick={onSettingsOpen}
                >
                  Settings
                </button>
                <button
                  className="py-3"
                  onClick={onLogOut}
                  style={{ height: "100%", textAlign: "center" }}
                >
                  Logout
                </button>
              </div>
              <svg
                className="w-8 h-8"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
          ) : (
            <div className={`buttonscont mr-3`}>
              <a href="/login" className="langbtn !no-underline">
                Login/SignUp
              </a>
            </div>
          )}
          </div>
        </div>
      </div>
    </>
  );
}
