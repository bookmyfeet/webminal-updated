import BrowserOnly from "@docusaurus/BrowserOnly";
import React, { useEffect, useState } from "react";
import Terminal from "../components/Terminal";
import { User } from "@supabase/supabase-js";
import { supabase } from "../lib/supabase";
import { useHistory } from "@docusaurus/router";
import { userContext } from "../lib/context";

export default function Root({ children }: { children: React.ReactNode }) {
  const [user, setUser] = useState<User | null>(
    supabase.auth.session()?.user ?? null
  );
  const history = useHistory();

  useEffect(() => {
    const isFirstTime = async (id: string) => {
      if (!id) return false;
      const { count, error } = await supabase
        .from("profiles")
        .select("id", { count: "exact" })
        .eq("id", id);
      return !Boolean(count);
    };

    const { data: sub } = supabase.auth.onAuthStateChange((event, session) => {
      switch (event) {
        case "SIGNED_IN":
          setUser(session?.user ?? null);
          isFirstTime(user!.id).then((yes) => {
            if (yes) history.push("/settings", { firstTime: true });
          });
          break;
        case "SIGNED_OUT":
          setUser(null);
          history.push("/");
          break;
        default:
          setUser(session?.user ?? null);
          isFirstTime(user!.id).then((yes) => {
            if (yes) history.push("/settings", { firstTime: true });
          });
          break;
      }
    });

    if (user) {
      isFirstTime(user.id).then((yes) => {
        if (yes) history.push("/settings", { firstTime: true });
      });
    }

    return () => sub?.unsubscribe();
  }, []);

  return (
    <div className="background-hero">
    <userContext.Provider value={user}>
      {children}
      <BrowserOnly>{() => <Terminal />}</BrowserOnly>
    </userContext.Provider>
    </div>
  );
}
