/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.*", "./docs/**/*.*", "./blog/**/*.*"],
  theme: {
    extend: {},
  },
  plugins: ['docusaurus-tailwindcss'],
};
